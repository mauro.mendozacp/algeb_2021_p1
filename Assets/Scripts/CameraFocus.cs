﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFocus : MonoBehaviour
{
    [SerializeField] private float mouseSensitive;

    // Update is called once per frame
    void Update()
    {
        Cursor.lockState = CursorLockMode.Locked;

        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");

        float rotX = mouseX * mouseSensitive;
        float rotY = mouseY * mouseSensitive;

        Vector3 rot = Camera.main.transform.rotation.eulerAngles;

        rot.y += rotX;
        rot.x -= rotY;

        Camera.main.transform.rotation = Quaternion.Euler(rot);
    }
}
