﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MathDebbuger;

public class Test : MonoBehaviour
{
    public enum Ejercicio { uno, dos, tres, cuatro, cinco, seis, siete, ocho, nueve, diez };
    public Ejercicio ejercicio = Ejercicio.uno;

    public Color vectorColor = Color.red;

    public Vector3 A;
    public Vector3 B;
    private Vector3 C;

    // Start is called before the first frame update
    void Start()
    {
        Vector3Debugger.AddVector(A, Color.white, "A");
        Vector3Debugger.AddVector(B, Color.black, "B");
        Vector3Debugger.AddVector(C, vectorColor, "C");
    }

    private float aux = 0.0f;

    // Update is called once per frame
    void Update()
    {
        switch (ejercicio)
        {
            case Ejercicio.uno:
                C = B + A;
                break;
            case Ejercicio.dos:
                C = B - A;
                break;
            case Ejercicio.tres:
                C = A;
                C.Scale(B);
                break;
            case Ejercicio.cuatro:
                C = Vector3.Cross(B, A);
                break;
            case Ejercicio.cinco:
                aux += Time.deltaTime;
                C = Vector3.Lerp(A, B, aux);

                if (aux > 1.0f)
                {
                    aux = 0;
                }

                break;
            case Ejercicio.seis:
                C = Vector3.Max(A, B);
                break;
            case Ejercicio.siete:
                C = Vector3.Project(A, B.normalized);
                break;
            case Ejercicio.ocho:
                C = (A + B).normalized * Vector3.Distance(A, B);
                break;
            case Ejercicio.nueve:
                C = Vector3.Reflect(A, B.normalized);
                break;
            case Ejercicio.diez:
                aux += Time.deltaTime;
                C = Vector3.LerpUnclamped(B, A, aux);

                if (aux > 10.0f)
                {
                    aux = 0;
                }
                break;
            default:
                C = Vector3.zero;
                break;
        }

        Vector3Debugger.UpdatePosition("A", A);
        Vector3Debugger.UpdatePosition("B", B);
        Vector3Debugger.UpdatePosition("C", C);
        Vector3Debugger.UpdateColor("C", vectorColor);
    }
}
