﻿using UnityEngine;
using MathDebbuger;
using CustomMath;

public class EjerciciosVec3 : MonoBehaviour
{
    public enum Ejercicio { uno, dos, tres, cuatro, cinco, seis, siete, ocho, nueve, diez };
    public Ejercicio ejercicio = Ejercicio.uno;

    public Color vectorColor = Color.red;

    [SerializeField] Vector3 A;
    [SerializeField] Vector3 B;

    private Vec3 A2;
    private Vec3 B2;
    private Vec3 C;

    private float timer = 0.0f;
    private float ej5MaxLerp = 1.0f;
    private float ej10MaxLerp = 10.0f;

    // Start is called before the first frame update
    void Start()
    {
        A2 = new Vec3(A);
        B2 = new Vec3(B);

        Vector3Debugger.AddVector(A2, Color.white, "A");
        Vector3Debugger.AddVector(B2, Color.black, "B");
        Vector3Debugger.AddVector(C, vectorColor, "C");
    }

    // Update is called once per frame
    void Update()
    {
        A2.SetVec3(A);
        B2.SetVec3(B);

        switch (ejercicio)
        {
            case Ejercicio.uno:
                C = B2 + A2;

                break;
            case Ejercicio.dos:
                C = B2 - A2;

                break;
            case Ejercicio.tres:
                C = A2;
                C.Scale(B2);

                break;
            case Ejercicio.cuatro:
                C = Vec3.Cross(B2, A2);

                break;
            case Ejercicio.cinco:
                timer += Time.deltaTime;
                C = Vec3.Lerp(A2, B2, timer);

                if (timer > ej5MaxLerp)
                    timer = 0;

                break;
            case Ejercicio.seis:
                C = Vec3.Max(A2, B2);

                break;
            case Ejercicio.siete:
                C = Vec3.Project(A2, B2.normalized);

                break;
            case Ejercicio.ocho:
                C = (A2 + B2).normalized * Vec3.Distance(A2, B2);

                break;
            case Ejercicio.nueve:
                C = Vec3.Reflect(A2, B2.normalized);

                break;
            case Ejercicio.diez:
                timer += Time.deltaTime;
                C = Vec3.LerpUnclamped(B2, A2, timer);

                if (timer > ej10MaxLerp)
                    timer = 0;

                break;
            default:
                C = Vec3.Zero;

                break;
        }

        Vector3Debugger.UpdatePosition("A", A2);
        Vector3Debugger.UpdatePosition("B", B2);
        Vector3Debugger.UpdatePosition("C", C);
        Vector3Debugger.UpdateColor("C", vectorColor);
    }
}
