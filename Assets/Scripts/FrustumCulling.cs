﻿using CustomMath;
using System.Collections.Generic;
using UnityEngine;

public class FrustumCulling : MonoBehaviour
{
    [SerializeField] int meshLayerInt;

    Camera cameraFocus;
    Vec3 cameraPosition;
    Vec3 cameraDir;

    Vec3 point;

    Vec3 pointFront;
    Vec3 pointBack;
    Vec3 pointTopLeft;
    Vec3 pointTopRight;
    Vec3 pointBottonLeft;
    Vec3 pointBottonRight;

    PlaneTest up;
    PlaneTest down;
    PlaneTest left;
    PlaneTest right;
    PlaneTest front;
    PlaneTest back;

    GameObject cubeBack;
    GameObject cubeFront;
    GameObject cubeTopLeft;
    GameObject cubeTopRight;
    GameObject cubeBottonLeft;
    GameObject cubeBottonRight;

    private List<GameObject> listMeshes = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        cameraFocus = Camera.main;
        cameraDir = new Vec3(cameraFocus.transform.forward);
        cameraPosition = new Vec3(cameraFocus.transform.position);

        pointBack = new Vec3(cameraPosition + cameraDir * cameraFocus.nearClipPlane);
        pointFront = new Vec3(cameraPosition + cameraDir * cameraFocus.farClipPlane);

        float angleY = cameraFocus.fieldOfView / 2;
        float halfHeight = (Mathf.Tan(angleY * Mathf.PI / 180)) * cameraFocus.farClipPlane;
        float halfWidth = halfHeight * cameraFocus.aspect;

        pointTopLeft = new Vec3(pointFront + (cameraFocus.transform.up * halfHeight) - (cameraFocus.transform.right * halfWidth));
        pointTopRight = new Vec3(pointFront + (cameraFocus.transform.up * halfHeight) + (cameraFocus.transform.right * halfWidth));
        pointBottonLeft = new Vec3(pointFront - (cameraFocus.transform.up * halfHeight) - (cameraFocus.transform.right * halfWidth));
        pointBottonRight = new Vec3(pointFront - (cameraFocus.transform.up * halfHeight) + (cameraFocus.transform.right * halfWidth));

        up = new PlaneTest(pointTopLeft, pointTopRight, pointBack);
        down = new PlaneTest(pointBottonLeft, pointBottonRight, pointBack);
        left = new PlaneTest(pointTopLeft, pointBottonLeft, pointBack);
        right = new PlaneTest(pointTopRight, pointBottonRight, pointBack);
        front = new PlaneTest((pointBack - pointFront).normalized, pointFront);
        back = new PlaneTest((pointFront - pointBack).normalized, pointBack);

        cubeBack = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cubeBack.name = "Cube Back";
        cubeFront = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cubeFront.name = "Cube Front";
        cubeTopLeft = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cubeTopLeft.name = "Cube Top Left";
        cubeTopRight = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cubeTopRight.name = "Cube Top Right";
        cubeBottonLeft = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cubeBottonLeft.name = "Cube Botton Left";
        cubeBottonRight = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cubeBottonRight.name = "Cube Botton Right";

        GameObject[] objects = FindObjectsOfType<GameObject>();
        foreach (GameObject g in objects)
        {
            if (g.layer == meshLayerInt)
            {
                listMeshes.Add(g);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        UpdatePlanes();
        ViewMeshes();
    }

    void UpdatePlanes()
    {
        cameraDir.SetVec3(cameraFocus.transform.forward);
        cameraPosition.SetVec3(cameraFocus.transform.position);

        pointBack = cameraPosition + cameraDir * cameraFocus.nearClipPlane;
        pointFront = cameraPosition + cameraDir * cameraFocus.farClipPlane;

        float angleY = cameraFocus.fieldOfView / 2;
        float halfHeight = (Mathf.Tan(angleY * Mathf.PI / 180)) * cameraFocus.farClipPlane;
        float halfWidth = halfHeight * cameraFocus.aspect;

        pointTopLeft.SetVec3(pointFront + (cameraFocus.transform.up * halfHeight) - (cameraFocus.transform.right * halfWidth));
        pointTopRight.SetVec3(pointFront + (cameraFocus.transform.up * halfHeight) + (cameraFocus.transform.right * halfWidth));
        pointBottonLeft.SetVec3(pointFront - (cameraFocus.transform.up * halfHeight) - (cameraFocus.transform.right * halfWidth));
        pointBottonRight.SetVec3(pointFront - (cameraFocus.transform.up * halfHeight) + (cameraFocus.transform.right * halfWidth));

        up.Set3Points(pointTopLeft, pointTopRight, cameraPosition);
        down.Set3Points(pointBottonLeft, pointBottonRight, cameraPosition);
        left.Set3Points(pointTopLeft, pointBottonLeft, cameraPosition);
        right.Set3Points(pointTopRight, pointBottonRight, cameraPosition);
        front.SetNormalAndPosition((pointBack - pointFront).normalized, pointFront);
        back.SetNormalAndPosition((pointFront - pointBack).normalized, pointBack);

        up.Flip();
        right.Flip();

        cubeBack.transform.position = pointBack;
        cubeFront.transform.position = pointFront;
        cubeTopLeft.transform.position = pointTopLeft;
        cubeTopRight.transform.position = pointTopRight;
        cubeBottonLeft.transform.position = pointBottonLeft;
        cubeBottonRight.transform.position = pointBottonRight;
    }

    void ViewMeshes()
    {
        foreach (GameObject o in listMeshes)
        {
            bool pointInside = false;
            Vector3[] cubeVertices = o.GetComponent<MeshFilter>().mesh.vertices;

            for (int j = 0; j < cubeVertices.Length; j++)
            {
                point.SetVec3(o.transform.TransformPoint(cubeVertices[j]));

                if (up.GetSide(point) && down.GetSide(point) && left.GetSide(point) &&
                    right.GetSide(point) && front.GetSide(point) && back.GetSide(point))
                {
                    pointInside = true;
                    break;
                }
            }

            if (pointInside)
            {
                if (!o.activeSelf)
                {
                    o.SetActive(true);
                }
            }
            else
            {
                if (o.activeSelf)
                {
                    o.SetActive(false);
                }
            }
        }
    }
}
