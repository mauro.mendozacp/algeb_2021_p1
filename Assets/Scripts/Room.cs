﻿using CustomMath;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour
{
    Vec3 point;

    [SerializeField] private GameObject cube;

    [SerializeField] private Transform pointUp;
    [SerializeField] private Transform pointDown;
    [SerializeField] private Transform pointLeft;
    [SerializeField] private Transform pointRight;
    [SerializeField] private Transform pointFront;
    [SerializeField] private Transform pointBack;

    PlaneTest up;
    PlaneTest down;
    PlaneTest left;
    PlaneTest right;
    PlaneTest front;
    PlaneTest back;

    // Start is called before the first frame update
    void Start()
    {
        Vec3 pointUpVec3 = new Vec3(pointUp.position);
        Vec3 pointDownVec3 = new Vec3(pointDown.position);
        Vec3 pointLeftVec3 = new Vec3(pointLeft.position);
        Vec3 pointRightVec3 = new Vec3(pointRight.position);
        Vec3 pointFrontVec3 = new Vec3(pointFront.position);
        Vec3 pointBackVec3 = new Vec3(pointBack.position);

        up = new PlaneTest(Vec3.Down, pointUpVec3);
        down = new PlaneTest(Vec3.Up, pointDownVec3);
        left = new PlaneTest(Vec3.Right, pointLeftVec3);
        right = new PlaneTest(Vec3.Left, pointRightVec3);
        front = new PlaneTest(Vec3.Back, pointFrontVec3);
        back = new PlaneTest(Vec3.Forward, pointBackVec3);
    }

    // Update is called once per frame
    void Update()
    {
        point.SetVec3(cube.transform.position);

        if (up.GetSide(point) && down.GetSide(point) && left.GetSide(point) &&
                    right.GetSide(point) && front.GetSide(point) && back.GetSide(point))
        {
            Debug.Log("El cubo ESTA dentro del cuarto.");
        }
        else
            Debug.Log("El cubo NO ESTA dentro del cuarto.");
    }
}
