﻿using UnityEngine;
using CustomMath;

public struct PlaneTest
{
    private Vec3 normal;
    private float distance;

    public Vec3 Normal
    {
        get => normal;
        set => normal = value;
    }

    public float Distance
    {
        get => distance;
        set => distance = value;
    }

    public PlaneTest(Vec3 inNormal, Vec3 inPoint)
    {
        normal = Vec3.Normalize(inNormal);
        distance = -Vec3.Dot(normal, inPoint);
    }

    public PlaneTest(Vec3 inNormal, float d)
    {
        normal = Vec3.Normalize(inNormal);
        distance = d;
    }

    public PlaneTest(Vec3 a, Vec3 b, Vec3 c)
    {
        normal = Vec3.Normalize(Vec3.Cross(b - a, c - a));
        distance = -Vec3.Dot(normal, a);
    }

    public void SetNormalAndPosition(Vec3 inNormal, Vec3 inPoint)
    {
        normal = Vec3.Normalize(inNormal);
        distance = -Vec3.Dot(inNormal, inPoint);
    }

    public void Set3Points(Vec3 a, Vec3 b, Vec3 c)
    {
        normal = Vec3.Normalize(Vec3.Cross(b - a, c - a));
        distance = -Vec3.Dot(normal, a);
    }

    public void Flip()
    {
        normal = -normal; distance = -distance;
    }

    public PlaneTest flipped
    {
        get { return new PlaneTest(-normal, -distance); }
    }

    public void Translate(Vec3 translation)
    {
        distance += Vec3.Dot(normal, translation); 
    }

    public static PlaneTest Translate(PlaneTest plane, Vec3 translation)
    {
        return new PlaneTest(plane.normal, plane.distance += Vec3.Dot(plane.normal, translation));
    }

    public Vec3 ClosestPointOnPlane(Vec3 point)
    {
        var pointToPlaneDistance = Vec3.Dot(normal, point) + distance;
        return point - (normal * pointToPlaneDistance);
    }

    public float GetDistanceToPoint(Vec3 point) 
    { 
        return Vec3.Dot(normal, point) + distance;
    }

    public bool GetSide(Vec3 point)
    {
        return Vec3.Dot(normal, point) + distance > 0.0F;
    }

    public bool SameSide(Vec3 inPt0, Vec3 inPt1)
    {
        float d0 = GetDistanceToPoint(inPt0);
        float d1 = GetDistanceToPoint(inPt1);
        return (d0 > 0.0f && d1 > 0.0f) || (d0 <= 0.0f && d1 <= 0.0f);
    }

    public bool Raycast(Ray ray, out float enter)
    {
        float vdot = Vec3.Dot(new Vec3(ray.direction), normal);
        float ndot = -Vec3.Dot(new Vec3(ray.origin), normal) - distance;

        if (Mathf.Approximately(vdot, 0.0f))
        {
            enter = 0.0F;
            return false;
        }

        enter = ndot / vdot;

        return enter > 0.0F;
    }
}
